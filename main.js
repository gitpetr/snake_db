var game = {
  width: 640,
  height: 360,
  ctx: undefined,
  AND: {},
  AND_NOT: {},
  NO: {},
  OR: {},
  OR_NOT: {},
  XOR: {},

  sprites: {
    background: undefined,
    AND: undefined,
    AND_NOT: undefined,
    NO: undefined,
    OR: undefined,
    OR_NOT: undefined,
    XOR: undefined
  },

  init() {
    var canvas = document.getElementById("mycanvas");
    this.ctx = canvas.getContext("2d");
  },
 
  load() {
    for (var key in this.sprites) {
      this.sprites[key] = new Image();
      this.sprites[key].src = `images/${key}.gif`;
    }
  },

  start() {
    this.init();
    this.load();
    this.run();
  },

  render() {
    this.ctx.clearRect(0, 0, this.width, this.height);

    this.ctx.drawImage(this.sprites.background, 0, 0);
    this.ctx.drawImage(this.sprites.AND, this.AND.width * this.AND.frame, 0, 165, 142, this.AND.x, this.AND.y, this.AND.width, this.AND.height);
    this.ctx.drawImage(this.sprites.AND_NOT, this.AND_NOT.x, this.AND_NOT.y, this.AND_NOT.width, this.AND_NOT.height);
    this.ctx.drawImage(this.sprites.NO, this.NO.x, this.NO.y, this.NO.width, this.NO.height);
    this.ctx.drawImage(this.sprites.OR, this.OR.width * this.OR.frame, 0, 163, 148, this.OR.x, this.OR.y, this.OR.width, this.OR.height);
    this.ctx.drawImage(this.sprites.OR_NOT, this.OR_NOT.x, this.OR_NOT.y, this.OR_NOT.width, this.OR_NOT.height);
    this.ctx.drawImage(this.sprites.XOR, this.XOR.x, this.XOR.y, this.XOR.width, this.XOR.height);
  },
  run() {
    this.render();
    window.requestAnimationFrame(() => {
      this.run();
    });
  }
};

game.AND = { 
  width: 60,
  height: 60,
  frame: 0,
  x: 30,
  y: 30
}

game.AND_NOT = {
  width: 60,
  height: 60,
  frame: 0,
  x: 90,
  y: 30
}

game.NO = {
  width: 60,
  height: 60,
  frame: 0,
  x: 150,
  y: 30
}

game.OR = {
  width: 60,
  height: 60,
  frame: 0,
  x: 210,
  y: 30
}

game.OR_NOT = {
  width: 60,
  height: 60,
  frame: 0,
  x: 270,
  y: 30
}

game.XOR = {
  width: 60,
  height: 60,
  x: 330,
  y: 30,

  input1: 0,
  input2: 0,
  output: 0,
  
  signal: function () {
    if ((this.input1 == 1 && this.input2 == 0) || (this.input1 == 0 && this.input2 == 1)) {
      this.output = 1
    } else {
      this.output = 0
    }
  }
}

window.addEventListener('load', function() {
  game.start();
})